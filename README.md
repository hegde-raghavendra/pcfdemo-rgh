# PCF config-server demo


**This project is a demo for pcf push using manifest.yml and using external git config.**

it uses the git repo https://gitlab.com/raghavhegdegithub/workspace-config for config

Clone this project and push it to PCF.

Here is more info regarding config server setup and pushing it to PCF.

https://docs.run.pivotal.io/spring-cloud-services/config-server/configuring-with-git.html

https://docs.cloudfoundry.org/devguide/deploy-apps/deploy-app.html

